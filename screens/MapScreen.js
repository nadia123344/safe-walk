import React from 'react'
import { ScrollView, StyleSheet, View, Alert } from 'react-native'
import MapView from 'react-native-maps'
import Marker from 'react-native-maps'
import { SearchBar } from 'react-native-elements';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import MapViewDirections from 'react-native-maps-directions';
import { Users } from '../constants/Users'
import {
  Text,
  Animated,
  Image,
  Dimensions,
  TouchableOpacity
} from "react-native";

const { width, height } = Dimensions.get("window");

const CARD_HEIGHT = height / 4;
const CARD_WIDTH = CARD_HEIGHT - 50;
//code copied from https://www.freecodecamp.org/news/how-to-integrate-maps-in-react-native-using-react-native-maps-5745490fe055/
class MapScreen extends React.Component {
    state = {
        search: '',
        start_location: {
            latitude: 49.240280,
            longitude: -123.044410
        },
        end_location: null,
        from: '',
        default_region: {
            latitude: 37.78825,
            longitude: -122.4324,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
        },
        to: ''
      };

      displayUsers = () => {
        console.log("clicked!")
    }


    selectDestination = (data, details = null) => {

        const latDelta = Number(details.geometry.viewport.northeast.lat) - Number(details.geometry.viewport.southwest.lat)
        const lngDelta = Number(details.geometry.viewport.northeast.lng) - Number(details.geometry.viewport.southwest.lng)

        let region = {
          latitude: details.geometry.location.lat,
          longitude: details.geometry.location.lng,
          latitudeDelta: latDelta,
          longitudeDelta: lngDelta
        };

        this.setState({
          end_location: {
            latitude: details.geometry.location.lat,
            longitude: details.geometry.location.lng,
          },
          region: region,
          to: this.refs.endlocation.getAddressText() // get the full address of the user's destination
        });

      }

      askToWalkWith = (user, callback) =>{
        Alert.alert(
          `Want to walk with ${user.title}?`,
          '',
          [
            {text: 'Yes', onPress: () => this.props.navigation.navigate('Personal')},
            {text: 'No', onPress: () => callback()}
          ],
          {cancelable: false},
        );
     }

     deleteUserCard = (card) => {
      console.log("delete user card ");
     }
      componentDidMount() {
        navigator.geolocation.getCurrentPosition(
            (position) => {
              var region = regionFrom(
                position.coords.latitude,
                position.coords.longitude,
                position.coords.accuracy
              );

              Geocoder.from({
                latitude: position.coords.latitude,
                longitude: position.coords.longitude
              })
              .then((response) => {

                this.from_region = region;

                this.setState({
                  start_location: {
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude
                  },
                  region: region,
                  from: response.results[0].formatted_address
                });

              });

            }
          );

      }

    render() {
        const { search } = this.state;
        return (
          <View style={styles.container}>
            <MapView style = {
                {
                    flex: 1
                }
            }
            showsUserLocation = {
                true
            }
            region={this.state.region}
            initialRegion={{
                latitude: 49.240280,
                longitude: -123.044410,
                latitudeDelta: 0.01,
                longitudeDelta: 0.01
            }}
            >
            {
            this.state.end_location &&
            <MapView.Marker
            coordinate={this.state.end_location}
            title={'User'}
            onPress = {
                this.displayUsers
            }
            />}
            <GooglePlacesAutocomplete
                ref="endlocation"
                placeholder='Where do you want to go?'
                minLength={5}
                returnKeyType={'search'}
                listViewDisplayed='auto'
                fetchDetails={true}
                onPress={this.selectDestination}

                query={{
                key: 'AIzaSyC6Q102PIVAgggqylEoA914tVla6-TPi10',
                language: 'en',
                }}

                styles={{
                textInputContainer: {
                    width: '100%',
                    backgroundColor: '#FFF'
                },
                listView: {
                    backgroundColor: '#FFF'
                }
                }}
                debounce={200}
            />
            {
            this.state.start_location && this.state.end_location &&
            <MapViewDirections
              origin={{
                'latitude': this.state.start_location.latitude,
                'longitude': this.state.start_location.longitude
              }}
              destination={{
                'latitude': this.state.end_location.latitude,
                'longitude': this.state.end_location.longitude
              }}
              strokeWidth={5}
              strokeColor={'black'}
              apikey={'AIzaSyC6Q102PIVAgggqylEoA914tVla6-TPi10'}
            />
            }
            {Users.map((user, index) => {
              return (
                <MapViewDirections
                key={index}
                origin={user.start_location}
                destination={user.end_location}
                strokeWidth={3}
                strokeColor={user.color}
                apikey={'AIzaSyC6Q102PIVAgggqylEoA914tVla6-TPi10'}
                />
              );
            })}
            {Users.map((user, index) => {
              return (
                <MapView.Marker
                key={index}
                coordinate={user.start_location}
                title={user.title}
                pinColor={user.color}
                />
              );
            })}
            </MapView>
             <Animated.ScrollView
             horizontal
             scrollEventThrottle={1}
             showsHorizontalScrollIndicator={false}
             snapToInterval={CARD_WIDTH}
             onScroll={Animated.event(
               [
                 {
                   nativeEvent: {
                     contentOffset: {
                       x: this.animation,
                     },
                   },
                 },
               ],
               { useNativeDriver: true }
             )}
             style={styles.scrollView}
             contentContainerStyle={styles.endPadding}
           >
             {Users.map((user, index) => (
               <TouchableOpacity key={index} onPress={() => this.askToWalkWith(user, this.deleteUserCard)}>
              <View style={styles.card} key={index}>
                 <Image
                   source={user.pic}
                   style={styles.cardImage}
                   resizeMode="cover"
                 />
                 <View style={styles.textContent}>
                   <Text numberOfLines={1} style={styles.cardtitle}>{user.title}</Text>
                   <Text numberOfLines={1} style={styles.cardDescription}>
                     {user.message}
                   </Text>
                 </View>
               </View>
               </TouchableOpacity>

             ))}
           </Animated.ScrollView>
          </View>
        )
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  scrollView: {
    position: "absolute",
    bottom: 30,
    left: 0,
    right: 0,
    paddingVertical: 10,
  },
  endPadding: {
    paddingRight: width - CARD_WIDTH,
  },
  card: {
    padding: 10,
    elevation: 2,
    backgroundColor: "#FFF",
    marginHorizontal: 10,
    shadowColor: "#000",
    shadowRadius: 5,
    shadowOpacity: 0.3,
    shadowOffset: { x: 2, y: -2 },
    height: CARD_HEIGHT,
    width: CARD_WIDTH,
    overflow: "hidden",
  },
  cardImage: {
    flex: 3,
    width: "100%",
    height: "100%",
    alignSelf: "center",
  },
  textContent: {
    flex: 1,
  },
  cardtitle: {
    fontSize: 12,
    marginTop: 5,
    fontWeight: "bold",
  },
  cardDescription: {
    fontSize: 12,
    color: "#444",
  },
  markerWrap: {
    alignItems: "center",
    justifyContent: "center",
  },
  marker: {
    width: 8,
    height: 8,
    borderRadius: 4,
    backgroundColor: "rgba(130,4,150, 0.9)",
  },
  ring: {
    width: 24,
    height: 24,
    borderRadius: 12,
    backgroundColor: "rgba(130,4,150, 0.3)",
    position: "absolute",
    borderWidth: 1,
    borderColor: "rgba(130,4,150, 0.5)",
  },
});

export default MapScreen