import shuffleArray from '../utils/shuffleArray'

export const Users = shuffleArray([
  {
    start_location: {latitude: 49.240280,
                    longitude: -123.044410},
    end_location: {latitude: 49.2827,
                    longitude: -123.1207},
    pic: require('../assets/images/women/women1.jpg'),
    title: 'Amelia, 27',
    message: "Thanks for walking with me!",
    color : 'olive'
  },
  {
    start_location: {latitude: 49.240280,
                    longitude: -123.044410},
    end_location: {latitude: 49.2827,
                    longitude: -123.1207},
    pic: require('../assets/images/women/women2.jpg'),
    title: 'Joanna, 19',
    message: "Let me know when if you need someone to walk home with tonight!",
    color : 'purple'
  },
  {
    start_location: {latitude: 49.278094,
        longitude: -122.919883},
    end_location: {latitude: 49.2827,
        longitude: -123.1207},
    pic: require('../assets/images/women/women3.jpg'),
    title: 'Charlie, 32',
    message: 'I am about to walk home right now',
    color: 'red'
  },

  {
    start_location: {latitude: 48.278094,
        longitude: -122.919883},
    end_location: {latitude: 49.2827,
        longitude: -122.1207},
    pic: require('../assets/images/women/women5.jpg'),
    title: 'Lucy, 27',
    message: 'Yes i am doing the same route as last time',
    color: 'gray'
  },
  {
    start_location: {latitude: 49.228094,
        longitude: -122.919883},
    end_location: {latitude: 49.2827,
        longitude: -122.1407},
    pic: require('../assets/images/women/women4.jpg'),
    title: 'Mary, 23',
    message: "Dont walk through east van, thats a sketch area",
    color: 'teal'
  },
  {
    start_location: {latitude: 49.178094,
        longitude: -122.919883},
    end_location: {latitude: 49.2827,
        longitude: -123.1407},
    pic: require('../assets/images/women/women14.jpg'),
    title: 'Michelle, 45',
    message: 'Maybe we can stop by Lucky Donut on the walk home',
    color: 'aqua'
  },
  {
    start_location: {latitude: 48.278094,
        longitude: -122.919883},
    end_location: {latitude: 49.2827,
        longitude: -123.5207},
    pic: require('../assets/images/women/women12.jpg'),
    title: 'Arya, 18',
    message: 'Not today!',
    color: 'yellow'
  },
])
