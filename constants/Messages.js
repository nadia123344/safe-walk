import shuffleArray from '../utils/shuffleArray'

export const Messages = shuffleArray([
  {
    pic: require('../assets/images/women/women1.jpg'),
    title: 'Amelia, 27',
    message: "Let's get to your favorite restaurant.",
  },
  {
    pic: require('../assets/images/women/women2.jpg'),
    title: 'Joanna, 19',
    message: "What's the best way to win you over?",
  },
  {
    pic: require('../assets/images/women/women3.jpg'),
    title: 'Charlie, 32',
    message: 'How about we go for a coffee on Sunday?',
  },

  {
    pic: require('../assets/images/women/women5.jpg'),
    title: 'Lucy, 27',
    message: 'Sleeping for now.',
  },
  {
    pic: require('../assets/images/women/women4.jpg'),
    title: 'Mary, 23',
    message: "Hey, what's up?",
  },
  {
    pic: require('../assets/images/women/women14.jpg'),
    title: 'Michelle, 45',
    message: 'Howdy!!!',
  },
  {
    pic: require('../assets/images/women/women12.jpg'),
    title: 'Arya, 18',
    message: 'Not today!',
  },
])
