import shuffleArray from '../utils/shuffleArray'

export const HomeScreenPics = shuffleArray([
  {
    pic: require('../assets/images/women/women1.jpg'),
    title: 'Amelia, 27',
    caption: '16 miles away',
  },
  {
    pic: require('../assets/images/women/women2.jpg'),
    title: 'Joanna, 19',
    caption: '2 miles away',
  },
  {
    pic: require('../assets/images/women/women3.jpg'),
    title: 'Charlie, 32',
    caption: '24 miles away',
  },
  {
    pic: require('../assets/images/women/women4.jpg'),
    title: 'Mary, 23',
    caption: '45 miles away',
  },
  {
    pic: require('../assets/images/women/women5.jpg'),
    title: 'Lucy, 27',
    caption: '32 miles away',
  },
  {
    pic: require('../assets/images/women/women6.jpg'),
    title: 'Rachel, 29',
    caption: '30 miles away',
  },
  {
    pic: require('../assets/images/women/women7.jpg'),
    title: 'Ava, 31',
    caption: '14 miles away',
  },
  {
    pic: require('../assets/images/women/women8.jpg'),
    title: 'Monica, 35',
    caption: '19 miles away',
  },
  {
    pic: require('../assets/images/women/women9.jpg'),
    title: 'Lisa, 25',
    caption: '7 miles away',
  },
  {
    pic: require('../assets/images/women/women10.jpg'),
    title: 'Julia, 22',
    caption: '9 miles away',
  }
])

export const TopPicksScreenPics = shuffleArray([
  {
    pic: require('../assets/images/women/women11.jpg'),
    title: 'Annie, 40',
    caption: '26h left',
  },
  {
    pic: require('../assets/images/women/women12.jpg'),
    title: 'Lena, 31',
    caption: '20h left',
  },
  {
    pic: require('../assets/images/women/women13.jpg'),
    title: 'Kendra, 19',
    caption: '15h left',
  },
  {
    pic: require('../assets/images/women/women14.jpg'),
    title: 'Mia, 23',
    caption: '45h left',
  },
  {
    pic: require('../assets/images/women/women15.jpg'),
    title: 'Jenny, 27',
    caption: '12h left',
  }
])
